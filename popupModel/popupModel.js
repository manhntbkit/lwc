import { LightningElement,api } from 'lwc';

export default class PopupModel extends LightningElement {
    @api isVisible;
    handleCancel(){
        this.isVisible = false;
        //reset data to default value if necessary.
        var cancelEvent = new CustomEvent('cancel');
        this.dispatchEvent(cancelEvent);
    }

    handleSave(){
        this.isVisible = false;
        //reset data to default value if necessary.
        var cancelEvent = new CustomEvent('save');
        this.dispatchEvent(cancelEvent);
    }

}