import { LightningElement, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';

export default class FormMultipleStepsAction extends LightningElement {
    @track currentStep;

    connectedCallback(){
        //this.toggleTwoAndThreeSteps();
        //this.currentStep =  "step1";
    }

    goToStepTwo() {
        this.toggleOneAndTwoSteps();
        this.currentStep =  "step2";
    }

    goToStepThree() {
        this.toggleTwoAndThreeSteps();
        this.currentStep =  "step3";
    }
    goBackToStepOne() {
        this.toggleOneAndTwoSteps();
        this.currentStep =  "step1";
    }
    goBackToStepTwo() {
        this.toggleTwoAndThreeSteps();
        this.currentStep =  "step2";
    }
    handleSuccess() {
        var event = new ShowToastEvent({
            title:'Success!',
            variant:'success',
            message: 'The record has been updated successfully.'
        });
        this.dispatchEvent(event);
    }

    ///
    toggleOneAndTwoSteps() {
        var stepOne = this.template.querySelector(".stepOne");
        stepOne.classList.toggle('slds-hide');

        var stepTwo = this.template.querySelector(".stepTwo");
        stepTwo.classList.toggle('slds-hide');
    }
    
    toggleTwoAndThreeSteps() {
        var stepTwo = this.template.querySelector(".stepTwo");
        if(stepTwo){
            stepTwo.classList.toggle('slds-hide');
        }

        var stepThree = this.template.querySelector(".stepThree");
        if(stepThree){
            stepThree.classList.toggle('slds-hide');
        }
        
	}

}