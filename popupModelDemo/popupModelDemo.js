import { LightningElement, track } from 'lwc';

export default class PopupModelDemo extends LightningElement {
    @track isModelVisible;
    openModel(){
        this.isModelVisible = true;
    }

    // onCancel(){
    //     this.isModelVisible = false;
    //     //reset data to default value if necessary.
    // }

    handleCancel(){
        this.isModelVisible = false;
    }

    handleSave(){
        this.isModelVisible = false;
    }


}