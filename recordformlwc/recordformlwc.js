import { LightningElement } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Account.Name';
import REVENUE_FIELD from '@salesforce/schema/Account.AnnualRevenue';
import INDUSTRY_FIELD from '@salesforce/schema/Account.Industry';

export default class Recordformlwc extends LightningElement {
    fields = [NAME_FIELD, REVENUE_FIELD, INDUSTRY_FIELD];

    handleSubmit(event){
        event.preventDefault();
        const fields = event.detail.fields;
        //fields.Name = 'Hello';
        //fields.Website  = 'Google.com';
        this.template.querySelector('lightning-record-form').submit(fields);
        
    }

}